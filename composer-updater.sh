#!/bin/sh

if ! git diff-index --quiet HEAD --; then
  echo "Creating new branch";
  BRANCH="composer-$(date +'%m-%d-%y-%H-%M-%S')";                          
  git checkout -b "$BRANCH";
  git add composer.lock;
  
  echo "Collecting diff info";
  chmod u+x ./composer-lock-diff;
  DESC="$(./composer-lock-diff --md)";

  echo "Committing new files";
  git commit -m "update composer.lock";
  git push;

  echo "Creating pull request";
  AUTH=$(echo -n "$BOT_NAME:$BOT_PASS" | base64);
  response=$(curl https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/pullrequests \
        -sb -v -f -X POST \
        -H 'Content-Type: application/json' \
        -H "Authorization: Basic $AUTH" \
        -d '{
               "title": "Merge in '"${BRANCH}"'", 
               "description": "'"${DESC}"'",
               "source": {
                   "branch": {
                     "name": "'"${BRANCH}"'"
                   }
               },
               "close_source_branch": "true"
            }');
  
  echo "Searching pull request link";
  pullrequest=$(echo $response | grep -oP "https://bitbucket.org/\w+/\w+/pull-requests/\d+\"");
  echo $pullrequest;
  
  echo "Sending message to Slack";
  curl "$SLACK_HOOK" \
          -sb -v -f -X POST \
          -H 'Content-Type: application/json' \
          -d '{
                 "channel": "#test-api-notify", 
                 "icon_url": "https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557058:84165188-23e1-43fc-b3e7-584ea785e902/e9eecbf4-211b-4b1c-8dc6-f539c8c4e7a7/128",
                 "text": "New Pull Request with composer update was created", 
                 "attachments": [{
                     "title": "'"${pullrequest}"',
                     "title_link": "'"${pullrequest}"',
                     "color": "#36a64f",
                     "text": "Bot"
                 }]
              }';
            
  echo "Finish";
fi;